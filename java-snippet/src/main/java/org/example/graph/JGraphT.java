package org.example.graph;

import org.jgrapht.Graph;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.List;

public class JGraphT {
    public static void main(String[] args) {
        Integer n1 = 1;
        Integer n2 = 2;
        Integer n3 = 3;
        Integer n4 = 4;
        Integer n5 = 5;
        Integer n6 = 6;
        Integer n7 = 7;
        Integer n8 = 8;

        Graph<Integer, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);
        graph.addVertex(n1);
        graph.addVertex(n2);
        graph.addVertex(n3);
        graph.addVertex(n4);
        graph.addVertex(n5);
        graph.addVertex(n6);
        graph.addVertex(n7);
        graph.addVertex(n8);
        graph.addEdge(n1, n2);
        graph.addEdge(n2, n3);
        graph.addEdge(n3, n4);
        graph.addEdge(n4, n5);
        graph.addEdge(n2, n6);
        graph.addEdge(n6, n7);
        graph.addEdge(n7, n8);

        System.out.println("graph=" + graph);


        DijkstraShortestPath<Integer, DefaultEdge> dijkstraShortestPath
                = new DijkstraShortestPath<>(graph);
        List<Integer> shortestPath = dijkstraShortestPath
                .getPath(n1,n8).getVertexList();
        System.out.println("shortestPath=" + shortestPath);
    }
}
