package org.example.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamOps {
    /**
     * 去重
     */
    private static void distinct() {
        List<Integer> ints = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        List<Integer> some = ints.stream().distinct().collect(Collectors.toList());
        System.out.println("distinct=" + some);
    }

    /**
     * 排序
     */
    private static void sort() {
        List<Integer> ints = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            ints.add((int) (Math.random() * 100));
        }
        System.out.println("raw=" + ints);
        List<Integer> sorted = ints.stream().sorted().collect(Collectors.toList());
        System.out.println("sorted=" + sorted);
    }

    /**
     *
     */
    private static void select() {
        List<Integer> ints = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            ints.add(i);
        }
        List<Integer> head = ints.stream().limit((long) (Math.random() * 10)).collect(Collectors.toList());
        System.out.println("head=" + head);
        List<Integer> tail = ints.stream().skip((long) (Math.random() * 100)).collect(Collectors.toList());
        System.out.println("tail=" + tail);

    }

    /**
     * map to each
     */
    private static void map() {
        List<Integer> ints = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        List<Double> doubles = ints.stream().map(each -> Math.pow(each, each)).collect(Collectors.toList());
        System.out.println("self multiply=" + doubles);
    }

    /**
     * 过滤
     */
    private static void filter() {
        List<String> strings = Arrays.asList("Foo", "", "FooBar", "", "F", "foobar");
        List<String> nonEmpty = strings.stream().filter(each -> each.length() > 3).collect(Collectors.toList());
        System.out.println("nonEmpty=" + nonEmpty);
    }
}
