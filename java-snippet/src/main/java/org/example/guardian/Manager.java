package org.example.guardian;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程管家, 负责启动工作线程与监听线程.
 *
 * @author KazuHo
 * @version 1.0
 * @since 2021/12/5 22:05
 */
public class Manager {
    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

    private Manager() {
    }

    public static Manager getInstance() {
        return Holder.MANAGER;
    }

    /**
     * 启动工作线程与监听线程.
     */
    public void start() {
        //  启动线程
        Monitor monitor = new Monitor();
        Executor executor = new Executor();
        executor.addObserver(monitor);
        EXECUTOR.execute(executor);
    }

    private static class Holder {
        private static final Manager MANAGER = new Manager();
    }
}
