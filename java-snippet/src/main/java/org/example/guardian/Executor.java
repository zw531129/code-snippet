package org.example.guardian;

import java.util.Observable;

/**
 * 工作线程, 继承Observable, 当线程猝死时上报告警.
 *
 * @author KazuHo
 * @version 1.0
 * @since 2021/12/5 22:04
 */
public class Executor extends Observable implements Runnable {
  @Override
  public void run() {
    while (true) {
      try {
        // 死循环, 永远执行业务方法
        System.out.println("do something");
      } catch (Exception e) {
        e.printStackTrace();
        alarm();
        break;
      }
    }
  }

  private void alarm() {
    // 更新状态, 通知observer
    super.setChanged();
    notifyObservers();
  }
}
