package org.example.guardian;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程监视器, 负责拉起挂了的线程.
 *
 * @author KazuHo
 * @version 1.0
 * @since 2021/12/5 22:04
 */
public class Monitor implements Observer {
  @Override
  public void update(Observable o, Object arg) {
    String which = o.getClass().getSimpleName().toUpperCase();
    ExecutorService executorService = Executors.newSingleThreadExecutor();
    Monitor monitor = new Monitor();
    //  死了哪个拉哪个
    switch (which) {
      case "something":
        // 拉起X线程
        System.out.printf("pulling up something.%n");
        break;
      case "other":
        // 拉起Y线程
        System.out.printf("pulling up other.%n");
        break;
      default:
        // 拉起线程
        Executor exe = new Executor();
        exe.addObserver(monitor);
        executorService.execute(exe);
        System.out.printf("pulling up default executor.%n");
    }
  }
}
