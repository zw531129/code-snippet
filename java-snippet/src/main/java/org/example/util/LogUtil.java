package org.example.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 日志记录工具, 包装了JDK自带的日志系统, 启动时通过静态方法设置日志等级, 懒得用第三方日志的可以凑合用下.
 *
 * @author KazuHo
 * @version 1.0
 * @since 2021-08-16
 */
public final class LogUtil {
    private static final Logger LOGGER;

    static {
        LOGGER = Logger.getGlobal();
        LOGGER.setLevel(levelMapping(System.getProperty("LOG_LEVEL", "WARN")));
    }

    private LogUtil() {
    }

    private static Level levelMapping(String level) {

        switch (level) {
            case "ERROR":
                return Level.SEVERE;
            case "WARN":
                return Level.WARNING;
            case "INFO":
                return Level.INFO;
            default:
                return Level.OFF;
        }
    }
}
