package org.example.util;

import com.google.gson.*;

import java.util.Objects;

/**
 * Json格式化工具类.
 *
 * @author KazuHo
 * @version 1.0
 * @since 2021/12/5 23:20
 */
public final class JsonUtil {
    private static final Gson GSON = new GsonBuilder().disableHtmlEscaping().create();

    private JsonUtil() {
    }

    /**
     * 对象转JSON字符串, 底层使用GSON实现, 封装丰富定位信息
     *
     * @param object Java对象
     * @return JSON字符串
     */
    public static String toJson(Object object) {
        Objects.requireNonNull(object);
        try {
            return GSON.toJson(object);
        } catch (JsonIOException e) {
            System.err.printf("unable to serialize %s \n to json", object.getClass().getCanonicalName());
            throw e;
        }
    }

    /**
     * JSON字符串转换为Java对象
     *
     * @param json  JSON字符串
     * @param clazz 类
     * @param <T>   JavaBean类型
     * @return JavaBean
     */
    public static <T> T fromJson(String json, Class<T> clazz) {
        Objects.requireNonNull(json);
        try {
            return GSON.fromJson(json, clazz);
        } catch (JsonSyntaxException e) {
            System.err.printf("unable to parse json to %s\n", clazz.getCanonicalName());
            throw e;
        }
    }

    /**
     * JsonElement转换为Java对象
     *
     * @param jElement JsonElement
     * @param clazz    类
     * @param <T>      JavaBean类型
     * @return JavaBean
     */
    public static <T> T fromJson(JsonElement jElement, Class<T> clazz) {
        Objects.requireNonNull(jElement);
        try {
            return GSON.fromJson(jElement, clazz);
        } catch (JsonSyntaxException e) {
            System.err.printf("unable to parse json to %s \n", clazz.getCanonicalName());
            throw e;
        }
    }

    /**
     * 提取JsonObject中的String字段
     *
     * @param jObject jsonObject
     * @param field   字段名
     * @return 字段值
     */
    public static String getString(JsonObject jObject, String field) {
        Objects.requireNonNull(jObject);
        if (!jObject.has(field)) {
            return "";
        }
        Object value = jObject.get(field);
        if (Objects.isNull(value)) {
            System.err.printf("filed %s is null\n", field);
            return "";
        }

        return jObject.get(field).isJsonNull() ? "" : jObject.get(field).getAsString();
    }

    /**
     * 提取JsonObject中的int字段
     *
     * @param jObject jsonObject
     * @param field   字段名
     * @return 字段值
     */
    public static int getInt(JsonObject jObject, String field) {
        Objects.requireNonNull(jObject);
        if (!jObject.has(field)) {
            return 0;
        }
        Object value = jObject.get(field);
        if (Objects.isNull(value)) {
            System.err.printf("filed %s is null\n", field);
            return 0;
        }

        return jObject.get(field).isJsonNull() ? -1 : jObject.get(field).getAsInt();
    }

    /**
     * 提取JsonObject中的Json数组
     *
     * @param jObject jsonObject
     * @param field   字段名
     * @return Json数组
     */
    public static JsonArray getJsonArray(JsonObject jObject, String field) {
        Objects.requireNonNull(jObject);

        if (!jObject.has(field)) {
            return new JsonArray();
        }
        Object value = jObject.get(field);
        if (Objects.isNull(value)) {
            System.err.printf("filed %s is null\n", field);
            return new JsonArray();
        }

        return jObject.get(field).isJsonNull() ? new JsonArray() : jObject.get(field).getAsJsonArray();
    }

    /**
     * 提取JsonObject中的boolean字段
     *
     * @param jObject jsonObject
     * @param field   字段名
     * @return 字段值
     */
    public static boolean getBoolean(JsonObject jObject, String field) {
        Objects.requireNonNull(jObject);
        if (!jObject.has(field)) {
            return false;
        }
        Object value = jObject.get(field);
        if (Objects.isNull(value)) {
            System.err.printf("filed %s is null\n", field);
            return false;
        }

        return !jObject.get(field).isJsonNull() && jObject.get(field).getAsBoolean();
    }

    /**
     * 提取JsonObject中的boolean字段
     *
     * @param jObject jsonObject
     * @param field   字段名
     * @return 字段值
     */
    public static double getDouble(JsonObject jObject, String field) {
        Objects.requireNonNull(jObject);
        if (!jObject.has(field)) {
            return 0.0d;
        }
        Object value = jObject.get(field);
        if (Objects.isNull(value)) {
            System.err.printf("filed %s is null\n", field);
            return 0.0d;
        }

        return jObject.get(field).isJsonNull() ? 0.0d : jObject.get(field).getAsDouble();
    }
}
